package product;

import java.math.BigDecimal;

public class Product {
    private String name;
    private BigDecimal basePrice;
    private BigDecimal taxAmount = new BigDecimal(0);
    private BigDecimal pricePlusTaxAmount;



    public Product(String name, BigDecimal basePrice) {
        this.name = name;
        this.basePrice = basePrice;
        this.pricePlusTaxAmount = basePrice;
    }

    public void calculateTax() {

    }

    public String getName() {
        return name;
    }

    public BigDecimal getBasePrice() {
        return basePrice;
    }

    public BigDecimal getTaxAmount() {
        return taxAmount;
    }

    public void setTaxAmount(BigDecimal taxAmount) {
        this.taxAmount = taxAmount;
    }

    public void setPricePlusTaxAmount(BigDecimal pricePlusTaxAmount) {
        this.pricePlusTaxAmount = this.pricePlusTaxAmount.add(pricePlusTaxAmount);
    }

    public BigDecimal getPricePlusTaxAmount() {
        return pricePlusTaxAmount;
    }

    @Override
    public boolean equals(Object o) {
        return getName().equals(((Product) o).getName());
    }

    @Override
    public String toString() {
        return name;
    }
}

package product;

import java.math.BigDecimal;

public class NormalTaxedProduct extends TaxedProduct {
    private static final BigDecimal NORMAL_TAX_RATE = new BigDecimal(10);



    public NormalTaxedProduct(Product product) {
        super(product);
        setTaxRate(NORMAL_TAX_RATE);
    }

    public NormalTaxedProduct(Product product, BigDecimal modifiedTaxRate) {
        super(product, modifiedTaxRate);
    }
}

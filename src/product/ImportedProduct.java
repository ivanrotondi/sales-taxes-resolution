package product;

import java.math.BigDecimal;

public class ImportedProduct extends TaxedProduct {
    private static final BigDecimal IMPORT_TAX_RATE = new BigDecimal(5);



    public ImportedProduct(Product product) {
        super(product);
        setTaxRate(IMPORT_TAX_RATE);
    }

    public ImportedProduct(Product product, BigDecimal modifiedTaxRate) {
        super(product, modifiedTaxRate);
    }

    @Override
    public String toString() {
        return "imported " + super.toString();
    }
}

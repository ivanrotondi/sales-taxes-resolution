package product;

import utilities.MethodUtils;

import java.math.BigDecimal;

public abstract class TaxedProduct extends Product {
    private Product product;
    private BigDecimal taxRate = new BigDecimal(0);



    public TaxedProduct(Product product) {
        super(product.getName(), product.getBasePrice());
        this.product = product;
    }

    public TaxedProduct(Product product, BigDecimal modifiedTaxRate) {
        super(product.getName(), product.getBasePrice());
        this.product = product;
        setTaxRate(modifiedTaxRate);
    }

    @Override
    public void calculateTax() {
        // this calculation works for easier double solution ! !
        //product.setTaxAmount(((double) ((long) (amount * 20 + 0.99))) / 20);

        setTaxAmount(MethodUtils.calculateTaxWithFormat(taxRate, product.getBasePrice()));
        setPricePlusTaxAmount(getTaxAmount());

        product.calculateTax();
    }

    public void setTaxRate(BigDecimal taxRate) {
        this.taxRate = taxRate;
    }

    @Override
    public void setPricePlusTaxAmount(BigDecimal pricePlusTaxAmount) {
        super.setPricePlusTaxAmount(getTaxAmount());
        product.setPricePlusTaxAmount(pricePlusTaxAmount);
    }

    @Override
    public BigDecimal getTaxAmount() {
        return super.getTaxAmount().add(product.getTaxAmount());
    }

    @Override
    public BigDecimal getPricePlusTaxAmount() {
        return product.getPricePlusTaxAmount();
    }
}

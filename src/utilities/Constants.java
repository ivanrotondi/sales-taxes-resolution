package utilities;

public final class Constants {
    public static final String perfumeBottle = "bottle of perfume";
    public static final String musicCD = "music CD";
    public static final String chocolateBar = "chocolates bar";
    public static final String chocolateBox = "box of chocolates";
    public static final String headachePills = "packet of headache pills";
}

package utilities;

import java.math.BigDecimal;
import java.math.RoundingMode;

public final class MethodUtils {
    private static final BigDecimal roundingDecimal = new BigDecimal(0.5);
    private static final BigDecimal hundredBigDecimal = new BigDecimal(100);



    public static BigDecimal getStandardBigDecimal(BigDecimal value) {
        return value.setScale(2, RoundingMode.HALF_EVEN);
    }

    public static BigDecimal calculateTaxWithFormat(BigDecimal taxRate, BigDecimal basePrice) {
        return twoDigitRoundedValue(calculateTaxPercentage(taxRate, basePrice));
    }

    public static BigDecimal calculateTaxPercentage(BigDecimal taxRate, BigDecimal basePrice) {
        return taxRate.multiply(basePrice).divide(hundredBigDecimal, 3, RoundingMode.FLOOR);
    }

    public static BigDecimal twoDigitRoundedValue(BigDecimal value) {
        BigDecimal val1 = value.divide(roundingDecimal, 1, RoundingMode.UP);
        return val1.multiply(roundingDecimal);
    }
}

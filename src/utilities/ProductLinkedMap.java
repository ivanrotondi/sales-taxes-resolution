package utilities;

import java.util.LinkedHashMap;

public class ProductLinkedMap<K, T> extends LinkedHashMap<K, T> {

    @Override
    public T put(K k, T t) {
        for (int i=0; i < keySet().size(); i++) {
            if (keySet().toArray()[i].toString().equals(k.toString())) {
                return super.put((K) keySet().toArray()[i], t);
            }
        }
        return super.put(k, t);
    }

    @Override
    public boolean containsKey(Object o) {
        for (K element : keySet()) {
            if (element.toString().equals(o.toString())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public T get(Object o) {
        for (int i=0; i < keySet().size(); i++) {
            if (keySet().toArray()[i].toString().equals(o.toString())) {
                return (T) values().toArray()[i];
            }
        }
        return super.get(o);
    }
}

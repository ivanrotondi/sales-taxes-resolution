import cart.Cart;
import product.ImportedProduct;
import product.NormalTaxedProduct;
import product.Product;
import utilities.Constants;
import utilities.MethodUtils;

import java.math.BigDecimal;

public class MainJava{

    public static void main(String[] args) {
        System.out.println("INPUT:");
        System.out.println("");


        /*
            Input 1
         */
        Product i1BookOne = new Product("book", MethodUtils.getStandardBigDecimal(new BigDecimal(12.49)));
        Product i1BookTwo = new Product("book", MethodUtils.getStandardBigDecimal(new BigDecimal(12.49)));

        NormalTaxedProduct i1TaxedCd = new NormalTaxedProduct(new Product(Constants.musicCD, MethodUtils.getStandardBigDecimal(new BigDecimal(14.99))));

        Product i1ChocolateBar = new Product(Constants.chocolateBar, MethodUtils.getStandardBigDecimal(new BigDecimal(0.85)));

        Cart i1Cart = new Cart(1);
        i1Cart.addProductToCart(i1BookOne);
        i1Cart.addProductToCart(i1BookTwo);
        i1Cart.addProductToCart(i1TaxedCd);
        i1Cart.addProductToCart(i1ChocolateBar);

        i1Cart.printInput();


        /*
            Input 2
         */
        ImportedProduct i2ChocolateBox = new ImportedProduct(new Product(Constants.chocolateBox, MethodUtils.getStandardBigDecimal(new BigDecimal(10.00))));

        ImportedProduct i2PerfumeBottle = new ImportedProduct(new NormalTaxedProduct(new Product(Constants.perfumeBottle, MethodUtils.getStandardBigDecimal(new BigDecimal(47.50)))));

        Cart i2Cart = new Cart(2);
        i2Cart.addProductToCart(i2ChocolateBox);
        i2Cart.addProductToCart(i2PerfumeBottle);

        i2Cart.printInput();


        /*
            Input 3
         */
        ImportedProduct i3PerfumeBottleOne = new ImportedProduct(new NormalTaxedProduct( new Product(Constants.perfumeBottle, MethodUtils.getStandardBigDecimal(new BigDecimal(27.99)))));
        NormalTaxedProduct i3PerfumeBottleTwo = new NormalTaxedProduct(new Product(Constants.perfumeBottle, MethodUtils.getStandardBigDecimal(new BigDecimal(18.99))));

        Product headachePills = new Product(Constants.headachePills, MethodUtils.getStandardBigDecimal(new BigDecimal(9.75)));

        ImportedProduct i3ChocolateBoxOne = new ImportedProduct(new Product(Constants.chocolateBox, MethodUtils.getStandardBigDecimal(new BigDecimal(11.25))));
        ImportedProduct i3ChocolateBoxTwo = new ImportedProduct(new Product(Constants.chocolateBox, MethodUtils.getStandardBigDecimal(new BigDecimal(11.25))));
        ImportedProduct i3ChocolateBoxThree = new ImportedProduct(new Product(Constants.chocolateBox, MethodUtils.getStandardBigDecimal(new BigDecimal(11.25))));

        Cart i3Cart = new Cart(3);
        i3Cart.addProductToCart(i3PerfumeBottleOne);
        i3Cart.addProductToCart(i3PerfumeBottleTwo);
        i3Cart.addProductToCart(headachePills);
        i3Cart.addProductToCart(i3ChocolateBoxOne);
        i3Cart.addProductToCart(i3ChocolateBoxTwo);
        i3Cart.addProductToCart(i3ChocolateBoxThree);

        i3Cart.printInput();

        /*
            OUTPUTS
         */
        System.out.println("OUTPUT");
        System.out.println("");
        i1Cart.printOutput();
        i2Cart.printOutput();
        i3Cart.printOutput();
    }
}

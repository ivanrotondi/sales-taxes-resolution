package cart;

import product.Product;
import utilities.ProductLinkedMap;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.LinkedList;
import java.util.Map;

public class Cart {
    private BigDecimal total = new BigDecimal(0);
    private BigDecimal totalTaxes = new BigDecimal(0);
    private int cartNumber;

    private LinkedList<Product> cartedProducts;

    private ProductLinkedMap<Product, Integer> inputCustomLinkedMap;
    private ProductLinkedMap<Product, Integer> outputCustomLinkedMap;



    public Cart(int cartNumber) {
        this.cartNumber = cartNumber;
        cartedProducts = new LinkedList<>();
        inputCustomLinkedMap = new ProductLinkedMap<>();
        outputCustomLinkedMap = new ProductLinkedMap<>();
    }

    public void addProductToCart(Product product) {
        product.calculateTax();
        total = total.add(product.getPricePlusTaxAmount());
        totalTaxes = totalTaxes.add(product.getTaxAmount());
        cartedProducts.add(product);
    }

    public void printInput() {
        populateMap(inputCustomLinkedMap);

        printMap("Input " + cartNumber + ":", inputCustomLinkedMap, false);

        System.out.println("");
    }

    public void printOutput() {
        populateMap(outputCustomLinkedMap);

        printMap("Output " + cartNumber + ":", outputCustomLinkedMap, true);

        System.out.println("Sales Taxes: " + totalTaxes.setScale(2, RoundingMode.HALF_UP));
        System.out.println("Total: " + total.setScale(2, RoundingMode.HALF_UP));
        System.out.println("");
    }

    private void printMap(String title, ProductLinkedMap<Product, Integer> map, boolean isOutput) {
        System.out.println(title);
        for (Map.Entry<Product, Integer> entry : map.entrySet()) {
            BigDecimal productAmount = new BigDecimal(entry.getValue());
            String productName = entry.getKey().toString();
            BigDecimal productPrice;
            if (isOutput) {
                productPrice = entry.getKey().getPricePlusTaxAmount().multiply(productAmount).setScale(2, RoundingMode.HALF_UP);
                System.out.println(productAmount + " " + productName + ": " + productPrice);
            }
            else {
                productPrice = entry.getKey().getBasePrice();
                System.out.println(productAmount + " " + productName + " at " + productPrice);
            }
        }
    }

    private void populateMap(ProductLinkedMap<Product, Integer> map) {
        for (Product product : cartedProducts) {
            if (map.containsKey(product)) {
                Integer productAmount = map.get(product) + 1;
                map.put(product, productAmount);
            }
            else {
                map.put(product, 1);
            }
        }
    }
}
